#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <math.h>
#include "SpiceUsr.h"

void get_galilean_states( double epoch, char *ephemfile,
        SpiceDouble* ju_state,
        SpiceDouble* io_state,
        SpiceDouble* eu_state,
        SpiceDouble* ga_state,
        SpiceDouble* ca_state )
/* Extracts Jupiter and Io's positions (km) and velocity (km/s) in J2000 coordinates
 * from the supplied planetary ephemeris.
 * "io_state" and "jup_state" are assumed to have at least six elements each.
 */
{
    // Open ephemeris file
    double jd = epoch + 2400000.5;
    SpiceDouble et = (jd - j2000_c() ) * spd_c();
    SpiceDouble lt;

    furnsh_c( ephemfile );

    spkezr_c( "jupiter",  et, "j2000", "LT+S", "earth", ju_state, &lt );
    spkezr_c( "io",       et, "j2000", "LT+S", "earth", io_state, &lt );
    spkezr_c( "europa",   et, "j2000", "LT+S", "earth", eu_state, &lt );
    spkezr_c( "ganymede", et, "j2000", "LT+S", "earth", ga_state, &lt );
    spkezr_c( "callisto", et, "j2000", "LT+S", "earth", ca_state, &lt );

    unload_c( ephemfile );
}


int main( int argc, char *argv[] )
{
    // Check command line args
    if (argc < 2)
    {
        fprintf( stderr, "usage: %s [MJD]\n", argv[0] );
        exit(EXIT_FAILURE);
    }

    // Get Jupiter and Io positions
    double epoch = atof(argv[1]);

    // Print out header row
    printf( "# MJD ju_ra_rad ju_dec_rad " );
    printf(       "io_ra_rad io_dec_rad " );
    printf(       "eu_ra_rad eu_dec_rad " );
    printf(       "ga_ra_rad ga_dec_rad " );
    printf(       "ca_ra_rad ca_dec_rad\n" );

    char *ephemfile = "/usr/local/share/jpl/jup310.bsp";
    SpiceDouble ju_state[6],
                io_state[6],
                eu_state[6],
                ga_state[6],
                ca_state[6];
    get_galilean_states( epoch, ephemfile,
            ju_state,
            io_state,
            eu_state,
            ga_state,
            ca_state );

    // Print out RAs and Decs
    SpiceDouble RA, Dec, R;

    printf( "%e ", epoch );
    recrad_c( ju_state, &R, &RA, &Dec );  printf( "%e %e ",  RA, Dec );
    recrad_c( io_state, &R, &RA, &Dec );  printf( "%e %e ",  RA, Dec );
    recrad_c( eu_state, &R, &RA, &Dec );  printf( "%e %e ",  RA, Dec );
    recrad_c( ga_state, &R, &RA, &Dec );  printf( "%e %e ",  RA, Dec );
    recrad_c( ca_state, &R, &RA, &Dec );  printf( "%e %e\n", RA, Dec );
}

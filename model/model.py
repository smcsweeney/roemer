from math import sin, cos
import numpy as np
from scipy.optimize import curve_fit

# Mode Earth and Jupiter's orbits as circles centered on the sun
# From https://nssdc.gsfc.nasa.gov/planetary/factsheet/jupiterfact.html

# Legend:
#  E = Earth
#  J = Jupiter
#  I = Io
#  U = Europa
#  C = Callisto
#  G = Ganymede
#  M = generic Moon

#  r = orbital radius
#  v = orbital speed
#  w = angular speed
#  p = orbital phase
#  0 = orbital phase at time t0

#  t0 = reference time when Jupiter was at opposition

Er = 149.60e9 # m
Jr = 778.57e9 # m

Ev = 29.78e3 # m/s
Jv = 13.06e3 # m/s

Ew = Ev/Er # rad/s
Jw = Jv/Jr # rad/s

t0MJD = 58247.3527 # MJD

def MJD2sec(tMJD):
    """
    Converts an MJD time to seconds since t0.
    """
    return (tMJD - t0MJD)*86400

def orbital_vec(t, r, w, phase_at_t0):
    """
    Returns a 2-element vector representing a body's position (in its
    orbital plane). The coordinate system is defined such that at time t0,
    the Sun (at the origin), Earth, and Jupiter are all lined up along the
    positive x-axis. Time t is in seconds since t0.
    """
    phasor = r*np.*exp(1j*(t*w - phase_at_t0))
    return np.array([phasor.real, phasor.imag])

def Evec(t):
    return orbital_vec(t, Er, Ew, 0)

def Jvec(t):
    return orbital_vec(t, Jr, Jw, 0)

def EJ_dist(t):
    """
    Returns the distance (m) between the Earth and Jupiter at time t (seconds
    since t0).
    """
    theta = (Ew - Jw)*t
    dist  = np.sqrt(Er**2 + Jr**2 - 2*Er*Jr*cos(theta))
    return dist

def moon_ang_dist(t, Mr, Mw, M0, c):
    """
    Returns the angular distance between the moon and Jupiter, as seen from
    Earth, at time t (seconds since t0).
    """
    Mp = t*Mw - M0
    Mp_apparent = Mp - t*

# This script is used for aiding the conversion from hand-drawn diagrams of
# Jupiter and its moons to measurements of their positions.
#
# It is to be used in an iterative fashion. The first time it is run, it can
# be called with just a single argument which represents the date and time of
# the observation, in the format YYYYmmddTHHMMSS, where the 'T' is a literal
# 'T'. This will simply load and display the image file named
# YYYYmmddTHHMMSS.png, with the default position and scale settings. Once
# plotted, the location of the plot's origin can be noted and used in the next
# call to this script. Subsequently, the scale can then be fixed, and
# thereafter the positions of the visible moons.
#
# Usage:
# gnuplot -c {this_file} DATETIME [CENTRE_X CENTRE_Y OUTER_RING
#     MOON1X MOON1Y MOON2X MOON2Y MOON3X MOON3Y MOON4X MOON4Y]
#
# Example:
# 

usage = "gnuplot -c {this_file} DATETIME"

if (ARGC == 0) {
    print sprintf("%s: error: too few arguments\nusage: %s", ARG0, usage)
    exit
}

file_exists(file) = system("[ -f '".file."' ] && echo '1' || echo '0'") + 0

DATETIME = ARG1

PNG = DATETIME.".png"
MDAT = "measurements.dat"

if (!file_exists(PNG)) {
    print sprintf("%s: error: cannot find file %s", ARG0, PNG)
    exit
}

if (!file_exists(MDAT)) {
    print sprintf("%s: error: cannot find file %s", ARG0, MDAT)
    exit
}

DAT = system(sprintf('grep "^%s" %s', DATETIME, MDAT))

ARGC = words(DAT)

if (ARGC == 0) {
    printf sprintf("%s: error: cannot find entry for %s in %s", ARG0, DATETIME, MDAT)
    exit
}

CENTRE_X = 0.0
CENTRE_Y = 0.0
O_X      = 8.0
O_Y      = 8.0

set size ratio -1
unset key
set xrange [*:*] noextend
set yrange [*:*] noextend

if (ARGC >= 2) { CENTRE_X = -word(DAT,2) }
if (ARGC >= 3) { CENTRE_Y = -word(DAT,3) }
if (ARGC >= 4) { O_X      =  word(DAT,4) }
if (ARGC >= 5) { O_Y      =  word(DAT,5) }
if (ARGC >= 6) { MOON1X   =  word(DAT,6) }

PIXELSIZE_X = 8.0/O_X
PIXELSIZE_Y = 8.0/O_Y

if (ARGC <= 4) {
    plot PNG binary filetype=png \
                    origin=(CENTRE_X*PIXELSIZE_X,CENTRE_Y*PIXELSIZE_Y) \
                    dx=PIXELSIZE_X dy=PIXELSIZE_Y \
             with rgbimage
}

set samples 5000

if (ARGC == 5) {
    plot PNG binary filetype=png \
                    origin=(CENTRE_X*PIXELSIZE_X,CENTRE_Y*PIXELSIZE_Y) \
                    dx=PIXELSIZE_X dy=PIXELSIZE_Y \
             with rgbimage, \
         for [i=1:8]  sqrt(i**2 - x**2) lc rgb "red", \
         for [i=1:8] -sqrt(i**2 - x**2) lc rgb "red"
}

if (ARGC >= 6) {
    set print $moons
    do for [i=6:ARGC:2] {
        print sprintf("%s %s", word(DAT,i), word(DAT,i+1))
    }
    unset print

    plot PNG binary filetype=png \
                    origin=(CENTRE_X*PIXELSIZE_X,CENTRE_Y*PIXELSIZE_Y) \
                    dx=PIXELSIZE_X dy=PIXELSIZE_Y \
             with rgbimage, \
         for [i=1:8]  sqrt(i**2 - x**2) lc rgb "red", \
         for [i=1:8] -sqrt(i**2 - x**2) lc rgb "red", \
         $moons w p pt 7 ps 2 lc rgb "blue"
}

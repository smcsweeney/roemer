avg_io_period = 1.769137786; % days

eclipses = load('times-of-io-eclipse.txt');
days     = datenum(eclipses(:,1:6));
days     = days - days(1); % i.e. days since the first measured eclipse time
angles   = eclipses(:,7) - eclipses(:,8);

% Calculate how "off" the eclipse time is, using the known average period
rems = rem(days, avg_io_period);

plot(days, rems*60*24, 'rx');
ylabel('Error in predicted eclipse times (min)');
xlabel('Time (days)');
print('eclipses.png');

#!/bin/bash

outfile=data.dat
echo "# Time_secs AngDist_rad AngDistErr_rad" > $outfile

for jpg in *.JPG
do
    scalefactor=$(awk '/^'${jpg}'/ {print $5}' < exif.dat)
    seconds=$(awk '/^'${jpg}'/ {print $6}' < exif.dat)
    awk '/^'${jpg}'/ {print '$seconds', $2*'${scalefactor}', $3*'${scalefactor}';
                      if ($4 != "") { print '$seconds', $4*'${scalefactor}', $5*'${scalefactor}' }
                      if ($6 != "") { print '$seconds', $6*'${scalefactor}', $7*'${scalefactor}' }
                      if ($8 != "") { print '$seconds', $8*'${scalefactor}', $9*'${scalefactor}' }
                     }' < measurements.dat >> $outfile
done

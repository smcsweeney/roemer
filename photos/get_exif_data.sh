#!/bin/bash

output=exif.dat
echo "# Filename DateTimeOriginal FocalLengthIn35mmFilm ExifImageWidth ScaleFactor_rad_per_px time_sec" > $output

for jpg in *.JPG
do
    echo "Extracting exif data from $jpg..."
    exif=$(identify -verbose "$jpg")
    dto=$(echo "$exif" | awk '/DateTimeOriginal/ {print gensub(":","-","g",$2) "T" $3}')
    fl35=$(echo "$exif" | awk '/FocalLengthIn35mmFilm/ {print $2}')
    w=$(echo "$exif" | awk '/ExifImageWidth/ {print $2}')
    scalefactor=$(echo "$fl35 $w" | awk '{print 2*atan2(35,(2*$1))/$2}')
    s=$(date -d "$dto" +"%s")
    echo -e "$jpg $dto $fl35 $w $scalefactor $s" >> $output
done
